<?php
/*
 * The template for displaying the footer.
 */
?>
		</div>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="width row">
			<div class="site-info columns small-12">
				<p class="copyright">&copy; All rights reserved.</p>
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<a class="exit-off-canvas"></a>
</div>
</div>

<?php wp_footer(); ?>

<script>
	$(document).foundation();
</script>
<script>
	$(function(){
		// Off canvas drop-down nav
		$('.off-canvas-list .menu-item-has-children').each(function(){
			var $item = $(this);
			var $ul = $item.children('ul');
			var $height = $ul.outerHeight();
			$item.addClass('row in').children('a').addClass('columns small-10').after("<a class='columns small-2 carets'><span class='fi-caret-top iconic-sm' title='caret top' aria-hidden='true'></span><span class='fi-caret-bottom iconic-sm' title='caret bottom' aria-hidden='true'></span></a>");
			$item.addClass('in');
			$ul.css('max-height', '0');
			$(this).children('.carets').click(function(){
				if($item.hasClass('in')){
					$ul.css('max-height', $height);
				}
				if($item.hasClass('out')){
					$ul.css('max-height', '0');
				}
				$item.toggleClass('in out');
			});
		});
		
		// Programmatic lightbox
		$(".entry-content a[href$='.jpg'], .entry-content a[href$='.gif'], .entry-content a[href$='.png']").fluidbox();	
		
		$(window).on("open.boxer", function(){
			$('html').addClass('boxer-active');
		});
		$(window).on("close.boxer", function(){
			$('html').removeClass('boxer-active');
		})
	});
</script>
</body>
</html>
