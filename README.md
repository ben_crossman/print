This is designed to be a good, simple starting point for building almost any project. As with all themes, assumptions are made, but I've tried to keep these to a minimum.

One of your first jobs should be making a new menu and adding it to the provided location (unless you're doing something more elaborate). Without doing this the off-canvas mobile nav will not work correctly.

The provided JavaScript and CSS files are meant to be optional. Currently the off-canvas nav uses Iconic fonts, this is the only exception; without it the carets will not display. But you should be able to use it alongside other icon fonts quite easily.

Overall documentation of Underscores should still apply. I've tended to comment out unneeded code rather than remove it entirely, but I did alter the structure of the markup a fair bit.