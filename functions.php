<?php
/*
 * This theme's functions and definitions
 *
 */

/*
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'print_setup' ) ) :
/*
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function print_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on print, use a find and replace
	 * to change 'print' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'print', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'print' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'print_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // print_setup
add_action( 'after_setup_theme', 'print_setup' );

/*
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function print_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'print' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'print_widgets_init' );

/*
 * Enqueue scripts and styles.
 */
function print_scripts() {
	
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/js/foundation.min.css' );
	
	wp_enqueue_style( 'normalize-css', get_template_directory_uri() . '/js/normalize.css' );
	
	wp_enqueue_style( 'print-style', get_stylesheet_uri() );
	
	wp_enqueue_style( 'iconic-css', get_template_directory_uri() . '/js/font/css/open-iconic-foundation.css' );	
	
	wp_enqueue_script( 'respond-js', get_template_directory_uri() . '/js/respond.js' );
	
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/js/foundation.min.js', array('jquery'), true );	
	
	wp_enqueue_script( 'print-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'print-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'rem-polyfill', get_template_directory_uri() . '/js/rem.min.js', true );
	
	wp_enqueue_script( 'matchheight-js', get_template_directory_uri() . '/js/jquery.matchHeight-min.js', array('jquery'), true );		
	
	//wp_enqueue_script( 'boxer-js', get_template_directory_uri() . '/js/jquery.fs.boxer.min.js', array('jquery'), true );
	
	//wp_enqueue_style( 'boxer-css', get_template_directory_uri() . '/js/jquery.fs.boxer.min.css' );	

	wp_enqueue_script( 'fluidbox-js', get_template_directory_uri() . '/js/jquery.fluidbox.min.js', array('jquery'), true );
	
	wp_enqueue_style( 'fluidbox-css', get_template_directory_uri() . '/js/fluidbox.css' );	

	wp_enqueue_script( 'wallpaper-js', get_template_directory_uri() . '/js/jquery.fs.wallpaper.min.js', array('jquery'), true );
	
	wp_enqueue_style( 'wallpaper-css', get_template_directory_uri() . '/js/jquery.fs.wallpaper.css' );	

	wp_enqueue_script( 'macaroon-js', get_template_directory_uri() . '/js/jquery.fs.macaroon.min.js', array('jquery'), true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
}
add_action( 'wp_enqueue_scripts', 'print_scripts' );

//Making jQuery Google API
function modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', false, '1.11.2');
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'modify_jquery');

require get_template_directory() . '/inc/foundation-walker-nav-menu.php';

// Add to body_class via ACF
//add_filter( 'body_class', 'my_class_names' );
//if (function_exists('get_field')){
//	function my_class_names( $classes ) {
//		$classes[] = strtr(get_field('page_style'), array('.' => '', ',' => ''));
//		return $classes;
//	}
//}//strtr($classesg, array('.' => '', ',' => ''));

/*
add_action('get_header', 'my_filter_head');

function my_filter_head() {
remove_action('wp_head', '_admin_bar_bump_cb');
}
*/

/*
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/*
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/*
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/*
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/*
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


add_theme_support('menus');

/*
http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples
*/
/*
register_nav_menus(array(
    'top-bar-l' => 'Left Top Bar', // registers the menu in the WordPress admin menu editor
    'top-bar-r' => 'Right Top Bar'
));
*/

/*
// the left top bar
function foundation_top_bar_l() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => 'menu',           		// class of container
        'menu' => '',                      	        // menu name
        'menu_class' => 'top-bar-menu left',         	// adding custom nav class
        'theme_location' => 'top-bar-l',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
    	'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new top_bar_walker()
	));
} // end left top bar

// the right top bar
function foundation_top_bar_r() {
    wp_nav_menu(array( 
        'container' => false,                           // remove nav container
        'container_class' => '',           		// class of container
        'menu' => '',                      	        // menu name
        'menu_class' => 'top-bar-menu right',         	// adding custom nav class
        'theme_location' => 'top-bar-r',                // where it's located in the theme
        'before' => '',                                 // before each link <a> 
        'after' => '',                                  // after each link </a>
        'link_before' => '',                            // before each link text
        'link_after' => '',                             // after each link text
        'depth' => 5,                                   // limit the depth of the nav
    	'fallback_cb' => false,                         // fallback function (see below)
        'walker' => new top_bar_walker()
	));
} // end right top bar
*/


/*
Customize the output of menus for Foundation top bar classes and add descriptions

http://www.kriesi.at/archives/improve-your-wordpress-navigation-menu-output
http://code.hyperspatial.com/1514/twitter-bootstrap-walker-classes/
*/
class top_bar_walker extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth, $args) {
        global $wp_query;
		
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array) $item->classes;
		
	$classes[] = ($item->current) ? 'active' : '';
        $classes[] = ($args->has_children) ? 'has-dropdown' : '';
		
	$args->link_before = (in_array('section', $classes)) ? '<label>' : '';
	$args->link_after = (in_array('section', $classes)) ? '</label>' : '';
	$output .= (in_array('section', $classes)) ? '<li class="divider"></li>' : '';

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args ) );
        $class_names = strlen(trim($class_names)) > 0 ? ' class="'.esc_attr($class_names).'"' : '';
		
	$output .= ($depth == 0) ? $indent.'<li class="divider"></li>' : '';
        $output .= $indent.'<li id="menu-item-'.$item->ID.'"'.$value.$class_names.'>';

        $attributes  = !empty($item->attr_title) ? ' title="' .esc_attr($item->attr_title).'"' : '';
        $attributes .= !empty($item->target)     ? ' target="'.esc_attr($item->target    ).'"' : '';
        $attributes .= !empty($item->xfn)        ? ' rel="'   .esc_attr($item->xfn       ).'"' : '';
        $attributes .= !empty($item->url)        ? ' href="'  .esc_attr($item->url       ).'"' : '';
		
	$item_output  = $args->before;
	$item_output .= (!in_array('section', $classes)) ? '<a'.$attributes.'>' : '';
	$item_output .= $args->link_before.apply_filters('the_title', $item->title, $item->ID);
	$item_output .= $args->link_after;
	$item_output .= (!in_array('section', $classes)) ? '</a>' : '';
	$item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
    function end_el(&$output, $item, $depth) {
        $output .= '</li>'."\n";
	}
    function start_lvl(&$output, $depth) {
        $indent  = str_repeat("\t", $depth);
        $output .= "\n".$indent.'<ul class="sub-menu dropdown">'."\n";
    }
    function end_lvl(&$output, $depth) {
        $indent  = str_repeat("\t", $depth);
        $output .= $indent.'</ul>'."\n";
    }		
    function display_element($element, &$children_elements, $max_depth, $depth=0, $args, &$output) {
        $id_field = $this->db_fields['id'];
        if (is_object($args[0])) {
            $args[0]->has_children = ! empty($children_elements[$element->$id_field]);
        }
        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }  	
} // end top bar walker

add_shortcode('wpbsearch', 'get_search_form');

?>