<?php
/*
 * header.php
 */
?><!DOCTYPE html>
<!--[if lt IE 9 ]><html <?php language_attributes(); ?> class="oldie"><![endif]-->
<!--[if (gt IE 8)|!(IE)]><!--><html <?php language_attributes(); ?> ><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<div class="off-canvas-wrap" data-offcanvas>
		<div class="inner-wrap">
			<div id="page" class="hfeed site">
				<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'print' ); ?></a>

				<div class="top-bar-container contain-to-grid hide-for-medium-up">
					<nav class="tab-bar" data-tabbar>
						<section class="tab-bar-section title-area">
	                        <h1 class="name">
		                        <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
	                        </h1>
						</section>
						<section class="right-small">
							<a class="right-off-canvas-toggle" href="#" aria-controls="menu" aria-expanded="false"><span class="fi-menu iconic-sm" title="menu" aria-hidden="true"></span> <!-- Menu --></a>
						</section>
					</nav>
		        </div>

				<header id="masthead" class="site-header" role="banner">
					<div class="width row">
						<div class="site-branding columns medium-3">
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php /*<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>*/?>
						</div><!-- .site-branding -->
						<div class="main-nav-holder columns medium-9">
							<nav class="main-navigation">
								<?php wp_nav_menu( array(
								        'theme_location'  => 'primary',
								            //'menu'            => '',
								            'menu_class'      => 'nav-menu',
								            //'menu_id'         => '',
								            'echo'            => true,
								            'fallback_cb'     => 'wp_page_menu',
								            //'before'          => '',
								            //'after'           => '',
								            //'link_before'     => '',
								            //'link_after'      => '',
								            //'items_wrap'      => '<ul id="%1$s" class="%2$s"><li><label>Menu</label></li>%3$s</ul>',
								            'depth'           => 0,
								        //'walker'          => new Foundation_Walker_Nav_Menu()
								        )
								    );
								?>
							</nav>
						</div>
					</div>
				</header><!-- #masthead -->

				<aside class="right-off-canvas-menu">
				<?php wp_nav_menu( array(
							        'theme_location'  => 'primary',
							            //'menu'            => '',
							            'container'       => 'div',
							            //'container_class' => 'off-canvas-list',
							            //'container_id'    => 'offcanvas-menu',
							            'menu_class'      => 'off-canvas-list',
							            //'menu_id'         => '',
							            'echo'            => true,
							            'fallback_cb'     => 'wp_page_menu',
							            //'before'          => '',
							            //'after'           => '',
							            //'link_before'     => '',
							            //'link_after'      => '',
							            'items_wrap'      => '<ul id="%1$s" class="%2$s"><li><label>Menu</label></li>%3$s</ul>',
							            'depth'           => 0,
							        'walker'          => new Foundation_Walker_Nav_Menu()
							        )
							    );
							?>
					<ul class="off-canvas utility-menu">
						<li>
							<a class="right-off-canvas-toggle" href="#" aria-controls="menu" aria-expanded="false"><span class="fi-circle-x iconic-sm" title="circle x" aria-hidden="true"></span> Close Menu</a>
						</li>
					</ul>
				</aside>

				<div id="content" class="site-content">
					<div class="width row">
