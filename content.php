<?php
/*
 * Content
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-template="content">
	<?php if (get_the_post_thumbnail()) : ?>
	<div class="thumbnail">
		<?php the_post_thumbnail('thumbnail') ?>
	</div>
	<?php endif; ?>
	<div class="text">
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php //print_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php $content = get_the_content();
			if ($content) : ?>
			<p>
			<?php echo wp_trim_words( $content , '25' ); ?>
			</p>
			<?php endif ?>
		<?php /*
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'print' ),
				'after'  => '</div>',
			) );
		*/?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php //print_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	</div>
</article><!-- #post-## -->